namespace Test.QuocTuan216
{
    public static class Extensions{
        public void SetText(this string value)
            => $"This called from SetText method with value {value}";
    }
}
